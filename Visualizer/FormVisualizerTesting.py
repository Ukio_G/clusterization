# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Visualizer/visualiser_testing/form.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(284, 132)
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setObjectName("gridLayout")
        self.add_word_to_visualizer = QtWidgets.QPushButton(Form)
        self.add_word_to_visualizer.setObjectName("add_word_to_visualizer")
        self.gridLayout.addWidget(self.add_word_to_visualizer, 2, 0, 1, 3)
        self.x_coord = QtWidgets.QDoubleSpinBox(Form)
        self.x_coord.setObjectName("x_coord")
        self.gridLayout.addWidget(self.x_coord, 1, 0, 1, 1)
        self.y_coord = QtWidgets.QDoubleSpinBox(Form)
        self.y_coord.setObjectName("y_coord")
        self.gridLayout.addWidget(self.y_coord, 1, 1, 1, 1)
        self.word = QtWidgets.QLineEdit(Form)
        self.word.setObjectName("word")
        self.gridLayout.addWidget(self.word, 0, 0, 1, 3)
        self.z_coord = QtWidgets.QDoubleSpinBox(Form)
        self.z_coord.setObjectName("z_coord")
        self.gridLayout.addWidget(self.z_coord, 1, 2, 1, 1)
        self.show_in_visualizer = QtWidgets.QPushButton(Form)
        self.show_in_visualizer.setObjectName("show_in_visualizer")
        self.gridLayout.addWidget(self.show_in_visualizer, 3, 0, 1, 3)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.add_word_to_visualizer.setText(_translate("Form", "Добавить слово по координатам"))
        self.show_in_visualizer.setText(_translate("Form", "Отобразить в визуализаторе"))


# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     Form = QtWidgets.QWidget()
#     ui = Ui_Form()
#     ui.setupUi(Form)
#     Form.show()
#     sys.exit(app.exec_())
