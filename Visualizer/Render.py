# -*- coding: utf-8 -*-
# Импортируем все необходимые библиотеки:
from OpenGL.GL import *
from ctypes import c_float,sizeof,c_uint

import glfw
import glm
import sys

import random
from array import array
import freetype
from PIL import Image

import string

import os.path
import os
import platform

pathname = ""

print(os.getcwd())
print(os.path.dirname(os.path.realpath(__file__)))

if platform.system() == "Windows":
	pathname = os.path.dirname(os.path.realpath(__file__)) + "\\glsl_src\\" 
elif platform.system() == "Linux":
	pathname = os.path.dirname(os.path.realpath(__file__)) + "/glsl_src/"

vertex_src = open(pathname+"vertex_primary.glsl", "r").readlines()
fragment_src = open(pathname+"fragment_primary.glsl", "r").readlines()

vertex_text_src = open(pathname+"vertex_text.glsl", "r").readlines()
fragment_text_src = open(pathname+"fragment_text.glsl", "r").readlines()

alphabet_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890йфяцычувскамепинртгоьшлбщдюзжхэъЙФЯЦЫЧУВСКАМЕПИНРТГОЬШЛБЩДЮЗЖЭХЪ" 

class stringTexture:
	def __init__(self,string):
		self.width = 0
		self.height = 0
		self.gl_texture = None
		self.bitmap_texture = list()
		self.texture_size = 64*128*2
		self.string = string
		self.path_to_font = pathname + "..\\IBMPlexMono-Text.ttf"

		self.texture_coordinates_per_character = dict()
		self.size_per_character = dict()

		self.create_bitmap_string()
		self.init_gl_texture()
	def init_gl_texture(self):
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
		self.gl_texture = glGenTextures(1)
		glBindTexture(GL_TEXTURE_2D, self.gl_texture)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, self.width, self.height, 0, GL_RED, GL_UNSIGNED_BYTE, self.bitmap_texture)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)


	def generate_uv_coordinates(self,char):
		start_point = self.texture_coordinates_per_character[char][0]
		end_point = self.texture_coordinates_per_character[char][1]

		uv_coord = [ 	[ end_point[0],   end_point[1]  ],
						[ end_point[0],   0.0 			],
						[ start_point[0], 0.0			],
						[ start_point[0], end_point[1] 	]
					]

		return uv_coord
	def create_bitmap_string(self):

		faces = list()

		total_width = max_height = 0.0

		for char in self.string:
			face = freetype.Face(self.path_to_font)
			face.set_char_size( 32*128)
			face.load_char(char)
			faces.append(face)
			size = (face.glyph.bitmap.width,face.glyph.bitmap.rows)
			self.size_per_character[char] = size

			coordinates = (glm.vec2(total_width,0.0),glm.vec2(total_width+face.glyph.bitmap.width,face.glyph.bitmap.rows))

			self.texture_coordinates_per_character[char] = coordinates

			total_width = total_width  + face.glyph.bitmap.width
			max_height = max([max_height, face.glyph.bitmap.rows])

		for key in self.texture_coordinates_per_character:
			self.texture_coordinates_per_character[key]
			start_position = self.texture_coordinates_per_character[key][0]
			end_position = self.texture_coordinates_per_character[key][1]
			
			coordinates_normalized = (	glm.vec2(start_position[0]/total_width,0.0),
										glm.vec2(end_position[0]/total_width,end_position[1]/max_height))
			self.texture_coordinates_per_character[key] = coordinates_normalized


		images = [Image.frombuffer('P',(x.glyph.bitmap.width,x.glyph.bitmap.rows),array('B',x.glyph.bitmap.buffer)) for x in faces]
		
		new_im = Image.new('P', (int(total_width), int(max_height)))

		x_offset = 0
		for im in images:
		  new_im.paste(im, (x_offset,0))
		  x_offset += im.size[0]

		self.width = total_width
		self.height = max_height
		self.bitmap_texture = new_im.tobytes()
		del new_im

class ShaderProgram(object):
    def __init__(self, vertex, fragment):
        self.program_id = glCreateProgram()
        vs_id = self.add_shader(vertex, GL_VERTEX_SHADER)
        frag_id = self.add_shader(fragment, GL_FRAGMENT_SHADER)

        glAttachShader(self.program_id, vs_id)
        glAttachShader(self.program_id, frag_id)
        glLinkProgram(self.program_id)

        if glGetProgramiv(self.program_id, GL_LINK_STATUS) != GL_TRUE:
            info = glGetProgramInfoLog(self.program_id)
            glDeleteProgram(self.program_id)
            glDeleteShader(vs_id)
            glDeleteShader(frag_id)
            raise RuntimeError('Error linking program: %s' % (info))
        glDeleteShader(vs_id)
        glDeleteShader(frag_id)

    def add_shader(self, source, shader_type):
        try:
            shader_id = glCreateShader(shader_type)
            glShaderSource(shader_id, source)
            glCompileShader(shader_id)
            if glGetShaderiv(shader_id, GL_COMPILE_STATUS) != GL_TRUE:
                info = glGetShaderInfoLog(shader_id)
                raise RuntimeError('Shader compilation failed: %s' % (info))
            return shader_id
        except:
            glDeleteShader(shader_id)
            raise

    def uniform_location(self, name):
        return glGetUniformLocation(self.program_id, name)

    def attribute_location(self, name):
        return glGetAttribLocation(self.program_id, name)

    def use(self):
    	glUseProgram(self.program_id)

class View:
	def __init__(self,xyz):
		self.x = xyz[0]
		self.y = xyz[1]
		self.z = xyz[2]
		self.eye_pos = glm.vec3(self.x,self.y,self.z);
		self.eye_target = glm.vec3(0.0,0.0,0.0);
		self.up_vector = glm.vec3(0.0,1.0,0.0);
		self.view_matrix = glm.lookAt(self.eye_pos, self.eye_target, self.up_vector)

	def reload(self):
		self.view_matrix = glm.lookAt(self.eye_pos, self.eye_target, self.up_vector)

	def camera_rotate(self,dx,dy):
		rotateMatrix = glm.rotate(glm.mat4(), glm.radians(dx), glm.vec3(0.0, 1.0, 0.0))
		self.eye_pos = glm.vec3(rotateMatrix * glm.vec4(self.eye_pos,0.0))
		self.reload()

	def camera_position_translate_y_axis(self,dx,dy):
		self.eye_pos = self.eye_pos + glm.vec3(0.0,float(dy/100),0.0)
		self.reload()

	def camera_position_translate_xy_plane(self,dx,dy):
		self.eye_target += glm.vec3(float(dx/100),0.0,float(dy/100))
		self.reload()

	def zoom(self,value):
		if value > 0:
			self.eye_pos = self.eye_pos * 1.1
		else:
			self.eye_pos = self.eye_pos * 0.9
		self.reload()

class Input:
	
	def __init__(self,parent):
		self.motion = False
		self.mouse_button = None
		self.oldX = 0
		self.oldY = 0
		self.parent = parent

		# Объект камера (экземпляр класса View)
		self.eye = self.parent.drawer.eye
		
		glfw.set_cursor_pos_callback(parent.window, self.cursor_position_callback)
		glfw.set_mouse_button_callback(parent.window,self.mouse_button_callback)
		glfw.set_scroll_callback(parent.window, self.scroll_callback)

	def mouse_button_callback(self,window,button,action,mods):
		if action == glfw.PRESS:
			self.motion = True
			self.mouse_button = button
			self.oldX = glfw.get_cursor_pos(window)[0]
			self.oldY = glfw.get_cursor_pos(window)[1]
		else:
			self.motion = False

	def scroll_callback(self, window, xoffset, yoffset):
		self.eye.zoom(yoffset)

	def cursor_position_callback(self,window, xpos,ypos):
		if self.motion is True:
			dx = self.oldX - xpos
			dy = -(self.oldY-ypos)

			if self.mouse_button == glfw.MOUSE_BUTTON_LEFT:
				self.eye.camera_rotate(dx,dy)

			elif self.mouse_button == glfw.MOUSE_BUTTON_RIGHT:
				self.eye.camera_position_translate_y_axis(dx,dy)
			
			elif self.mouse_button == glfw.MOUSE_BUTTON_MIDDLE:
				self.eye.camera_position_translate_xy_plane(dx,dy)

			self.oldX = xpos
			self.oldY = ypos


	def processInput(self):
		pass

class Window:
	
	def __init__(self):
		self.window = None
		self.callback_loop = lambda: None
		self.init_window()
		self.opengl_settings_apply()
		self.drawer = Drawer()
		self.input = Input(self)

	def init_window(self):
		if not glfw.init():
			print("GLFW initialization failed")
		glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR,3)
		glfw.window_hint(glfw.CONTEXT_VERSION_MINOR,3)
		glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
		self.window = glfw.create_window(640, 480, "3D data rendering", None, None)

		if not self.window:
			print("GLFW failed to initialized window")
			glfw.terminate()

		glfw.make_context_current(self.window)
		glfw.set_framebuffer_size_callback(self.window, self.fbo_callback)

		glViewport(0, 0, 640, 480);
		
		print('Vendor: %s' % (glGetString(GL_VENDOR)))
		print('Opengl version: %s' % (glGetString(GL_VERSION)))
		print('GLSL Version: %s' % (glGetString(GL_SHADING_LANGUAGE_VERSION)))
		print('Renderer: %s' % (glGetString(GL_RENDERER)))

	def fbo_callback(self,window,width,height):
		glViewport(0, 0, width, height)

	def opengl_settings_apply(self):
		glLineWidth(2)
		glPointSize(3)
		glEnable(GL_BLEND)
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
		glEnable(GL_DEPTH_TEST)
		glfw.set_window_should_close(self.window, False)

	def drawing_loop(self):
		while not glfw.window_should_close(self.window):
			glfw.poll_events()
			self.input.processInput()
			glClearColor(0.2, 0.2, 0.2, 1.0)
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

			self.drawer.draw_queue()
			
			glfw.swap_buffers(self.window)

		glfw.terminate()
		
class Drawer:
	def __init__(self):
		self.shader_primary = ShaderProgram(vertex_src,fragment_src)
		self.shader_text = ShaderProgram(vertex_text_src,fragment_text_src)
		self.line_width = 2
		self.point_size = 3
		self.projection_matrix = glm.perspective(3.14 * 0.25, 4.0 / 3.0, 0.1, 100.)
		self.eye = View([2.0,2.0,2.0])
		self.drawing_queue = list()	#List of Drawable objects - will drawed into draw loop

	def passing_uniforms(self,shader,model):
		glUniformMatrix4fv(shader.uniform_location("Model"),1,False,glm.value_ptr(model))
		glUniformMatrix4fv(shader.uniform_location("View"),1,False,glm.value_ptr(self.eye.view_matrix))
		glUniformMatrix4fv(shader.uniform_location("Projection"),1,False,glm.value_ptr(self.projection_matrix))

	def draw(self,drawable_object):

		if hasattr(drawable_object,"is_text"):
			current_shader = self.shader_text
		else:
			current_shader = self.shader_primary

		current_shader.use()
		self.passing_uniforms(current_shader,drawable_object.model_matrix)
		drawable_object.draw()

	def draw_queue(self):
		for drawable_object in self.drawing_queue:
			self.draw(drawable_object)

class Vertice:
	def __init__(self,position=list(),color=list(),texture_coordinates=list()):
		self.position = array("f",position)
		self.color = array("f",color)
		self.texture_coordinates = array("f",texture_coordinates)

class DrawableObject:

	def __init__(self,vertices=list(),indices=list()):
		self.vertices = vertices
		self.indices = indices
		self.vao = glGenVertexArrays(1)
		self.model_matrix = glm.mat4()
		self.last_buffer_index = 0
		self.buffers = list()

		self.positions = array('f',[])
		self.colors = array('f',[])
		self.texture_coordinates = array('f',[])

		self.from_vertices_to_arrays_set(vertices)
		print("from_vertices_to_arrays_set OK")
		self.initialize_object()
		print("initialize_object OK")

	def from_vertices_to_arrays_set(self,vertices):
		i = 0
		for vertice in vertices:
			print(i, " of ", vertices.__len__())
			self.positions.extend(vertice.position)
			self.colors.extend(vertice.color)
			self.texture_coordinates.extend(vertice.texture_coordinates)
			i += 1

	def initialize_object(self):
		glBindVertexArray(self.vao)

		self.create_ebo(self.indices)		
		self.create_vbo(self.positions)
		self.create_vbo(self.texture_coordinates,size=2)
		self.create_vbo(self.colors)

		glBindVertexArray(0)

	def create_ebo(self,indices):
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glGenBuffers(1))
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, array('i',indices).tobytes(), GL_STATIC_DRAW)

	def create_vbo(self,data,index=None,size=3,indices=list()):
		new_vbo = glGenBuffers(1)

		if index is None:
			index = self.last_buffer_index

		glBindBuffer(GL_ARRAY_BUFFER, new_vbo)
		glBufferData(GL_ARRAY_BUFFER, data.tobytes(), GL_STATIC_DRAW)

		glVertexAttribPointer(index, size, GL_FLOAT, False, sizeof(c_float)*size, None)
		glEnableVertexAttribArray(index)

		self.last_buffer_index = self.last_buffer_index + 1
		self.buffers.append(new_vbo)

class Plane(DrawableObject):
	def __init__(self,vertices=list(),indices=list()):
		self.using_texutre = True
		self.using_indices = True
		super(Plane,self).__init__(vertices,indices)
		self.texture = None
		self.size = glm.vec2(1.0,1.0)				# width, height
		self.position = glm.vec3(0.0,0.0,0.0) 		# x, y, z
		self.euler_angles = glm.vec3(0.0,0.0,0.0)	# roll, pitch, yaw

	def update_geometry(self):
		self.scale()
		self.rotate()
		self.translate()

	def rotate(self):
		pass

	def translate(self):
		self.model_matrix = glm.translate(self.model_matrix,glm.vec3(self.x,self.y,self.z))

	def scale(self):
		self.model_matrix = glm.scale(self.model_matrix,glm.vec3(1.0,self.height,self.width))


	def draw(self):
		if hasattr(self,"is_text"):
			if self.is_text is True and self.texture is not None:
				glBindTexture(GL_TEXTURE_2D, self.texture)

		glBindVertexArray(self.vao)
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, None)

class Basis(DrawableObject):
	def __init__(self,vertices=list()):
		self.using_color = True
		super(Basis,self).__init__(vertices)

	def draw(self):
		glBindVertexArray(self.vao)
		glDrawArrays(GL_LINES, 0, 6)
		glBindVertexArray(0)

class Points(DrawableObject):
	def __init__(self,vertices=list()):
		self.using_color = True
		super(Points, self).__init__(vertices)
		self.points_count = len(vertices)

	def draw(self):
		glBindVertexArray(self.vao)
		glDrawArrays(GL_POINTS, 0, self.points_count)
		glBindVertexArray(0)

class PlaneSet(DrawableObject):
	"""docstring for PlaneSet"""
	#planes не в смысле инстансы класса Plane, а в смысле 4 инстанса класса Vertice
	def __init__(self, planes_vertices=list()): 
		self.planes_indices = list()
		self.using_indices = True
		self.using_texutre = True
		self.planes_count = int(len(planes_vertices)/4)
		self.planes_indices = self.generate_indices()
		self.planes_vertices = planes_vertices
		
		super(PlaneSet, self).__init__(planes_vertices,self.planes_indices)

	def generate_indices(self):
		indices = []
		for x in range(0,self.planes_count):
			print("generating index ", x, " of ", self.planes_count)
			first = 4*x # Первый индекс плоскости
			second = first + 1
			third = first + 3
			fourth = first + 2
			# self.planes_indices = self.planes_indices + [first,second,third,second,fourth,third]
			indices.append(first)
			indices.append(second)
			indices.append(third)
			indices.append(second)
			indices.append(fourth)
			indices.append(third)
		return indices

	def add_plane_to_set(self,vertices=list()):
		self.planes_count = self.planes_count + 1
		self.planes_vertices = self.planes_vertices + vertices
		self.generate_indices()

	def draw(self):
		if hasattr(self,"is_text"):
			if self.is_text is True:
				glBindTexture(GL_TEXTURE_2D, self.texture.gl_texture)

		glBindVertexArray(self.vao)
		glDrawElements(GL_TRIANGLES, self.planes_count*6, GL_UNSIGNED_INT, None)

def plane_positions(width,height,position=[0.0,0.0,0.0],uv_coord=[[1.0,1.0],[1.0,0.0],[0.0,0.0],[0.0,1.0]]):
	vertices = list()
	vertices.append(Vertice([width/2+position[0] ,	 height/2+position[1],	position[2]],[],uv_coord[0]))
	vertices.append(Vertice([width/2+position[0] ,	-height/2+position[1],	position[2]],[],uv_coord[1]))
	vertices.append(Vertice([-width/2+position[0],	-height/2+position[1],	position[2]],[],uv_coord[2]))
	vertices.append(Vertice([-width/2+position[0],	 height/2+position[1],	position[2]],[],uv_coord[3]))
	return vertices

def planes_string(position,base_size,string,alphabet_texture):
	plane_indices = [0,1,3,1,2,3]
	planes = list()
	for i in range(0,len(string)):
		char = string[i]
		if not char in alphabet_chars:
			pass
		else:
			offset = [float(position[0] + base_size[0]*i),position[1],position[2]]
			vertices = plane_positions(base_size[0],base_size[1],offset,uv_coord=alphabet_texture.generate_uv_coordinates(char))
			planes =  planes + vertices
	return planes

# words_dictionary - словарь, где каждому слову соответствет трехкомпонентный список - координаты трехкомпонентного вектора
def thread_wrapper(words_dictionary):
	glfw_window = Window()


	points_vertices = list()
	planes_list = list()
	alphabet = stringTexture(alphabet_chars)

	# for point in points_list:
	# 	points_vertices.append(Vertice(point.position,[1.0,0.0,0.0],[]))
	# 	planes_list += planes_string(point.position,[0.1,0.1],point.str,alphabet)
	

	basis_vertices = [
	Vertice([0.0,0.0,0.0],[0.0,0.0,0.0],[]),
	Vertice([1.0,0.0,0.0],[1.0,0.0,0.0],[]),
	Vertice([0.0,0.0,0.0],[0.0,0.0,0.0],[]),
	Vertice([0.0,1.0,0.0],[0.0,1.0,0.0],[]),
	Vertice([0.0,0.0,0.0],[0.0,0.0,0.0],[]),
	Vertice([0.0,0.0,1.0],[0.0,0.0,1.0],[]),
	]

	for word,position in words_dictionary.items():
		points_vertices.append(Vertice(position,[1.0,0.0,0.0],[]))
		planes_list += planes_string(position,[0.02,0.02],word,alphabet)	


	points = Points(points_vertices)

	basis = Basis(basis_vertices)

	planeset= PlaneSet(planes_list)
	planeset.is_text = True
	planeset.texture = alphabet

	glfw_window.drawer.drawing_queue.append(points)
	glfw_window.drawer.drawing_queue.append(basis)
	glfw_window.drawer.drawing_queue.append(planeset)
	

	glfw_window.drawing_loop()

