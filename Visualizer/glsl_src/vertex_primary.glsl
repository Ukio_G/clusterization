#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec3 aColor;

uniform mat4 Model;
uniform mat4 Projection;
uniform mat4 View;

out vec3 outColor;
out vec2 TexCoord;

void main()
{
    gl_Position = Projection*View*Model*vec4(aPos.x, aPos.y, aPos.z, 1.0);
    TexCoord = aTexCoord;
    outColor = aColor;
}