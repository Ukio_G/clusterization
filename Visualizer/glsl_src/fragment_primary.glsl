#version 330 core
in vec3 outColor;
in vec2 TexCoord;

out vec4 FragColor;
uniform sampler2D ourTexture;


void main()
{
    FragColor = vec4(abs(outColor.r),abs(outColor.g*5),abs(outColor.b),1.0);
}
