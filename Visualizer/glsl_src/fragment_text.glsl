#version 330 core
in vec2 TexCoord;

out vec4 FragColor;
uniform sampler2D ourTexture;

void main()
{
	vec4 textureColor = texture(ourTexture, TexCoord);
    FragColor = vec4(textureColor.r,textureColor.r,textureColor.r,1.0);
    //FragColor = vec4(1.0,1.0,1.0,1.0);
}
