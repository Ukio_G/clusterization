# This Python file uses the following encoding: utf-8
import sys
# from .QtWidgets import QApplication, QWidget
from PyQt5.QtWidgets import QApplication, QWidget
from FormVisualizerTesting import Ui_Form
from Render import *
import threading

class PointedWord:
	def __init__(self,position=list(),string=""):
		self.position = position
		self.str = string

points_list = list()

class Tester(Ui_Form, QWidget):
	def __init__(self):
		super(Ui_Form, self).__init__()
		self.setupUi(self)
		self.add_word_to_visualizer.clicked.connect(self.add_word_to_visualizer_function)
		self.show_in_visualizer.clicked.connect(self.show_in_visualizer_function)

	def add_word_to_visualizer_function(self):
		global points_list
		points_list.append(PointedWord([self.x_coord.value(),self.y_coord.value(),self.z_coord.value()],self.word.text()))

		
	def show_in_visualizer_function(self):
		loop_thread = threading.Thread(target=glfw_main)
		loop_thread.start()		

def main():
	q_app = QApplication(sys.argv)
	t = Tester()
	t.show()
	q_app.exec_()

def glfw_main():
	global points_list
	glfw_window = Window()


	points_vertices = list()
	planes_list = list()
	alphabet = stringTexture(alphabet_chars)

	for point in points_list:
		points_vertices.append(Vertice(point.position,[1.0,0.0,0.0],[]))
		planes_list += planes_string(point.position,[0.1,0.1],point.str,alphabet)
	
	points = Points(points_vertices)
	planeset = PlaneSet(planes_list)
	planeset.is_text = True
	planeset.texture = alphabet

	glfw_window.drawer.drawing_queue.append(points)
	glfw_window.drawer.drawing_queue.append(planeset)
	glfw_window.drawing_loop()
	

main()