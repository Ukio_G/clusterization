from Render import *
import random
import string

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


class PointedWord:
	def __init__(self,position=list(),string=""):
		self.position = position
		self.str = string

def rebuild_drawing_queue():
	planeset = 	PlaneSet(global_planes_vertices)
	planeset.is_text = True
	planeset.texture = alphabet
	points = Points(global_points_vertices)

	glfw_window.drawer.drawing_queue.append(points)
	glfw_window.drawer.drawing_queue.append(planeset)

if __name__ == "__main__":
	
	points_list = []
	points_vertices = []
	planes_list = []
	_range = 10
	for i in range(1000):
		# global points_list
		x = random.uniform(-_range,_range)
		y = random.uniform(-_range,_range)
		z = random.uniform(-_range,_range)
		_str = randomString(int(random.uniform(10,20)))
		points_list.append(PointedWord([x,y,z],_str))

	print("Generated OK")
	glfw_window = Window()
	alphabet = stringTexture(alphabet_chars)

	for point in points_list:
		points_vertices.append(Vertice(point.position,[1.0,0.0,0.0],[]))
		planes_list += planes_string(point.position,[0.1,0.1],point.str,alphabet)
	print("List filling OK")

	points = Points(points_vertices)
	print("Points OK")
	print("planeset length is ", planes_list.__len__())
	planeset = PlaneSet(planes_list)
	print("PlaneSet OK")
	planeset.is_text = True
	planeset.texture = alphabet

	glfw_window.drawer.drawing_queue.append(points)
	print("Points appending OK")
	glfw_window.drawer.drawing_queue.append(planeset)
	print("PlaneSet appending OK")

	print("Basis appending")
	basis_vertices = [
	Vertice([0.0,0.0,0.0],[0.0,0.0,0.0],[]),
	Vertice([1.0,0.0,0.0],[1.0,0.0,0.0],[]),
	Vertice([0.0,0.0,0.0],[0.0,0.0,0.0],[]),
	Vertice([0.0,1.0,0.0],[0.0,1.0,0.0],[]),
	Vertice([0.0,0.0,0.0],[0.0,0.0,0.0],[]),
	Vertice([0.0,0.0,1.0],[0.0,0.0,1.0],[]),
	]

	basis = Basis(basis_vertices)
	
	print("Basis OK")
	
	print("Drawing loop starting")


	glfw_window.drawing_loop()
