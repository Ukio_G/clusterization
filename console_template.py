# ! /usr/bin/python3.6
# -*- coding: utf-8 -*-
from Workers import *
from Structures import *
from Application import *
from pprint import *


def main():
	dirname = "/home/ukio/clusterization/texts/Simple/"
	filenames = [str(dirname+str(i)) for i in range(1,10)]
	docs = filenames_to_documents_dictionary(filenames)
	fdocs = frequency_filter(docs)
	dictionary = corpora.Dictionary(fdocs)
	myCorpus = Corpus() 
	myCorpus.dictionary = dictionary
	myCorpus.filenames = filenames

	for vector in myCorpus:
		pprint(vector)

	# myCorpus.save()