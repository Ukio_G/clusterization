from collections import defaultdict
from pathlib import Path
from gensim import corpora
from os.path import expanduser
import configparser
from Workers import *
from Application import *
from Structures import *
from PyQt5.QtWidgets import QFileDialog
#from nltk.corpus import stopwords

class SettingsAdapter:
	def __init__(self,ui,settings):
		self.ui = ui
		self.settings = settings

	def apply_settings(self):
		self.settings.result_files_directory = self.ui.template_directory_path.text()
		self.settings.result_file_names = {
		'tokens_dictionary':self.ui.tokens_dictionary_filename.text(),
		'corpus':self.ui.corpus_filename.text(),
		'tf_idf_corpus':self.ui.tf_idf_corpus_filename.text(),
		}
		self.settings.is_stemming = self.ui.is_stemming.isChecked()
		self.settings.is_stoplist = self.ui.is_stoplist.isChecked()
		self.settings.is_exclude_minimum_frequency = self.ui.is_exclude_minimum_frequency.isChecked()
		self.settings.minimum_freq = self.ui.minimum_frequency.value()
		self.settings.stop_list = self.ui.stop_list.toPlainText().split(' ')
		self.settings.clusters_count = self.ui.clusters_count.value()
		self.settings.write_settings()

	def fill_settings_form(self):

		self.ui.template_directory_path.setText(self.settings.result_files_directory)
		self.ui.tokens_dictionary_filename.setText(self.settings.result_file_names.get('tokens_dictionary'))
		self.ui.corpus_filename.setText(self.settings.result_file_names.get('corpus'))
		self.ui.tf_idf_corpus_filename.setText(self.settings.result_file_names.get('tf_idf_corpus'))


		self.ui.is_stemming.setChecked(self.settings.is_stemming)
		self.ui.is_stoplist.setChecked(self.settings.is_stoplist)
		self.ui.is_exclude_minimum_frequency.setChecked(self.settings.is_exclude_minimum_frequency)

		self.ui.minimum_frequency.setValue(self.settings.minimum_freq)
		self.ui.clusters_count.setValue(self.settings.clusters_count)

		stoplist_string = ""
		for stoplist_element in self.settings.stop_list:
			stoplist_string += stoplist_element
			stoplist_string += " "

		self.ui.stop_list.setPlainText(stoplist_string)


import platform 

class Settings:
	"""docstring for Settings"""
	def __init__(self, parent, config_filename):
		self.config_filename = config_filename
		self.parent = parent
		self.read_settings()
		self.stop_list = []


	def read_settings(self):
		file_path = ""
		if platform.system() == "Linux":
			file_path = expanduser("~")+'/'+self.config_filename
		elif platform.system() == "Windows":
			file_path = expanduser("~")+'\\'+self.config_filename

		config_file = configparser.ConfigParser()
		config_file.read(file_path)
		self.clusters_count = int(config_file['DEFAULT'].get('clusters_count',0))
		self.language = config_file['DEFAULT'].get('language',"english")
		self.result_files_directory = config_file['DEFAULT'].get('result_files_directory',expanduser("~"))
		self.minimum_freq = int(config_file['DEFAULT'].get('minimum_freq',0))
		self.is_stemming = config_file['DEFAULT'].getboolean('is_stemming',True) # Приводить ли слова к начальной словоформе
		self.is_stoplist = config_file['DEFAULT'].getboolean('is_stoplist',True) # Исключать ли из документов стоп слова
		self.is_exclude_minimum_frequency = config_file['DEFAULT'].getboolean('is_exclude_minimum_frequency',True) # Исключать ли слова, которые встречаются реже некоторого значения (self.minimum_freq)

		if 'stop-list' in config_file:
			self.stop_list = config_file['stop-list'].get(self.language).split(' ')

		#self.stop_list = self.stop_list + stopwords.words('english')


		self.result_file_names = dict()
		if 'result_file_names' in config_file:
			for key in config_file['result_file_names']:
				self.result_file_names[key] = config_file['result_file_names'].get(key)

	def write_settings(self):
		file_path = ""
		if platform.system() == "Linux":
			file_path = expanduser("~")+'/'+self.config_filename
		elif platform.system() == "Windows":
			file_path = expanduser("~")+'\\'+self.config_filename

		config = configparser.ConfigParser()
		config['DEFAULT'] = {	'language':self.language,
								'result_files_directory':self.result_files_directory,
								'minimum_freq':self.minimum_freq,
								'clusters_count':self.clusters_count,
								'is_stemming':self.is_stemming,
								'is_stoplist':self.is_stoplist,
								'is_exclude_minimum_frequency':self.is_exclude_minimum_frequency,
								}

		stoplist_string = ""
		for stoplist_element in self.stop_list:
			stoplist_string += (stoplist_element + " ")

		config['stop-list'] = {}
		config['stop-list'][self.language] = stoplist_string

		config['result_file_names'] = {}

		for key in self.result_file_names:
			print(key, " = ",self.result_file_names[key])
			config['result_file_names'][key] = self.result_file_names[key]

		with open(file_path, 'w') as configfile:
			config.write(configfile)

class Document:
	"""Преобразуем текст из в список из слов. Если проще: Текст -> [слово, слово]"""
	def __init__(self,text):
		self.result = list()
	# Удаление стоп слов и приведение слов к базовой словоформе
	def prepare_text(self):
		pass

	def to_vec(self):
		pass

class InputFile:
	def __init__(self, file_path):
		self.file_path = file_path

	def to_memory_friendly_document(self):
		dictionary = corpora.Dictionary(line.lower().split() for line in open(self.file_path))
		return dictionary

class Corpus:
	def __init__(self):
		self.filenames = list()
		self.dictionary = corpora.Dictionary()

	def __iter__(self):
		from Workers import file_to_list_of_words
		for document in self.filenames:
			document_list = file_to_list_of_words(document)
			yield self.dictionary.doc2bow(document_list)			

	def save(self,filename):
		corpora.MmCorpus.serialize(filename, self)

	def load(self,filename):
		pass

class DataCache:
	def __init__(self,parent):
		self.parent = parent
		self.input_files = []

		self.documents = []
		self.dictionary = corpora.Dictionary()

		self.corpus = Corpus()

		self.tf_idf_model = models.TfidfModel()
		self.tf_idf_corpus = []

		# self.lsi_model = models.LsiModel()
		self.lsi_corpus = []
