import argparse

from PyQt5.QtWidgets import QFileDialog
from nltk.corpus.reader import documents
from pprint import pprint
from collections import defaultdict
from tfidf import TfIdf
from gensim import corpora, models
from nltk.stem import SnowballStemmer
from numpy import *

import numpy as np
import logging

from Application import *
from Structures import *

def filenames_to_documents_dictionary(filenames_list):
	documents = []
	for filename in filenames_list:
		document = file_to_list_of_words(filename)
		documents.append(document)

	return documents


##### ВРеменные функции, пока не определился, нужны они или нет

# функция убирает знаки препинания вокруг слова
def cleaning_word(word):
	word = word.lower()
	word = word.strip('.[]123456789!@#$%^&*()":;')
	return word

# 
def file_to_list_of_words(filename):
	from Application import Application
	words = []
	stoplist = Application().settings.stop_list
	lines = [line.strip() for line in open(filename) if line is not '\n']
	for line in lines:
		for word in line.split(" "):
			word = cleaning_word(word)
			if not word in words and not word in stoplist:
				words.append(word)
	return words



def frequency_filter(documents):
	from Application import Application
	frequency = defaultdict(int)
	texts = documents
	for text in texts:
	    for token in text:
	        frequency[token] += 1

	texts = [
	    [token for token in text if frequency[token] > Application().settings.minimum_freq]
	    for text in texts
	]

	return texts
