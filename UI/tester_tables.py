from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMainWindow, QPushButton, QWidget

from UI.Forms.tester_tables_form import Ui_Form
from PyQt5 import QtWidgets
import Structures


class TablesTester(Ui_Form, QWidget):
	def __init__(self, listWindows: list):
		super(Ui_Form, self).__init__()
		self.setupUi(self)
		
		self.listWindows = listWindows
		self.addCol.clicked.connect(self.addColClicked)
		self.addRow.clicked.connect(self.addRowClicked)
		#	self.addElem.clicked.connect(self.addElemClicked)
		
		self.remCol.clicked.connect(self.remColClicked)
		self.remRow.clicked.connect(self.remRowClicked)
		#	self.remElem.clicked.connect(self.remElemClicked)
		
		self.addRow.clicked.connect(self.update_table_data)
		self.addCol.clicked.connect(self.update_table_data)
		self.remCol.clicked.connect(self.update_table_data)
		self.remRow.clicked.connect(self.update_table_data)
		#	self.addElem.clicked.connect(self.self.update_table_data)
		
		self.selectedTable: QtWidgets.QTableWidget = None
		self.selectTable.currentIndexChanged.connect(self.changeTable)
		self.aboutSelected = self.aboutSelectedTable
		for window in listWindows:
			self.tablesToSelect(window)
	
	def tablesToSelect(self, window: QMainWindow):
		listTables = window.findChildren(QtWidgets.QTableWidget)
		for table in listTables:
			self.selectTable.addItem(table.objectName(), table)
	
	def addColClicked(self, num=0):
		self.selectedTable.insertColumn(num)
	
	def addRowClicked(self, num=0):
		self.selectedTable.insertRow(num)
	
	def remRowClicked(self, num=0):
		self.selectedTable.removeRow(num)
	
	def remColClicked(self, num=0):
		self.selectedTable.removeColumn(num)
	
	def update_table_data(self):
		rows = self.selectedTable.rowCount()
		cols = self.selectedTable.columnCount()
		
		rowsTItem = self.aboutSelected.itemAt(1, 3)
		colsTItem = self.aboutSelected.itemAt(1, 2)
		
		rowsTItem.setText(str(rows))
		colsTItem.setText(str(cols))
	
	def changeTable(self):
		self.selectedTable = self.selectTable.currentData(Qt.UserRole)
