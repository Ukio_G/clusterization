from collections import defaultdict

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMainWindow, QFileDialog
from gensim import corpora
from nltk import SnowballStemmer

import Structures
from UI.Forms.ngram_form import Ui_MainWindow

stemmer = SnowballStemmer('russian')
from gensim.models import Phrases


class NGramsWindow(Ui_MainWindow, QMainWindow):
	
	def __init__(self):
		super(Ui_MainWindow, self).__init__()
		self.setupUi(self)
		self.inputFile = None
		self.openFile_button.clicked.connect(self.openFile)
		self.openFile_action.triggered.connect(self.openFile)
		self.wordsPerNGramm.valueChanged.connect(self.nGramDetect)
		self.delimiterShow = True
	
	def openFile(self):
		self.inputFile = QFileDialog.getOpenFileName(self, 'Открыть входной файл')[0]
		self.inputFile = Structures.InputFile(self.inputFile)
		self.parsing()
		self.uw_t2id = self.unique_words().token2id
		self.clearTable(self.nGrammTableWidget)
		self.insertListToTable(self.nGrammTableWidget, self.inputFile.content_prepared)
		self.nGramDetect()
	
	def parsing(self):
		stoplist = set()
		strip = '\n,.\r'
		replace = set('\' \" ) ( 1 2 3 4 5 6 7 8 9 0 ! @ # $ % ^ & * \\ | / '.split())
		textcorrect = dict(strip=strip, stop=stoplist, replace=replace)
		self.splitPhrases(textcorrect)
		frequency = defaultdict(int)
		texts = self.inputFile.content_prepared
		for text in texts:
			for token in text:
				frequency[token] += 1
		result = self.parsingPhrasesWord(frequency)
		result = [x for x in result if x != []]
		self.inputFile.content_prepared = result
		return result
	
	def separatePhrases(self, textcorrect):  # Фразы не будут разбиты на отдельные слова, и каждая фраза будет обрабатываться как слово
		for document in self.inputFile.content:
			self.inputFile.content_prepared.append(
				[word.strip(textcorrect['strip']) for word in document.lower().replace(textcorrect['replace']).strip(textcorrect['strip']).split(',')
				 if word not in textcorrect['stop']])
	
	def splitPhrases(self, textcorrect):  # Фразы будут разбиты на отдельные слова и смешаны в общем словаре без какого либо разделения
		for document in self.inputFile.content:
			self.inputFile.content_prepared.append(
				[word.strip(textcorrect['strip']) for word in document.lower().replace(',', ' ').split() if word not in textcorrect['stop']])
	
	def parsingPhrasesWord(self, frequency):  # Получить и слова и фразы
		texts = [[token for token in text if frequency[token] > 1] for text in self.inputFile.content_prepared]
		return texts
	
	def parsingPhrasesOnly(self, frequency):  # Получить только фразы
		texts = [[token for token in text if frequency[token] > 1] for text in self.inputFile.content_prepared]
		return texts
	
	def unique_words(self):
		texts = self.inputFile.content_prepared
		dictionary = corpora.Dictionary(texts)
		return dictionary
	
	def nGramDetect(self):
		
		if self.inputFile is None:
			return
		
		if self.delimiterShow is True:
			delimiterSymbol = ' DELIMITER_' + str(self.wordsPerNGramm.value()) + ' '
		else:
			delimiterSymbol = ' '
		
		common_terms = set('в на и с для от при о а к та то'.split())
		textUniGram = self.inputFile.content_prepared.copy()
		dimensionalNGram = self.wordsPerNGramm.value()
		phrasesNGram = list()
		textNGram = [textUniGram]
		for i in range(0, dimensionalNGram):
			print(i)
			phrasesNGram.append(
				Phrases(textNGram[i], common_terms=common_terms, scoring='npmi', min_count=i + 2, threshold=0.5,
				        delimiter=(b' DELIMITER_' + bytes(str(i + 2), encoding='utf-8') + b' ', b' ')[self.delimiterShow is False]))
			
			textNGram.append([phrasesNGram[i][line] for line in textNGram[i]])
		
		ngrams = set()
		
		for item in textNGram[dimensionalNGram - 1]:
			for subitem in item:
				if subitem.find(delimiterSymbol) != -1:
					if subitem.count(delimiterSymbol) == 1:
						print('=====', subitem, '=====')
					# print('=====', subitem.replace(delimiterSymbol, ' '), '=====')
					else:
						print('+++++', subitem, '+++++')
					# print('+++++', subitem.replace(delimiterSymbol, ' '), '+++++')
					ngrams.add(subitem)
		
		self.clearTable(self.availableCompletion)
		self.insertListToTable(self.availableCompletion, [ngram for ngram in ngrams])
	
	def clearTable(self, table):
		table.setRowCount(0)
	
	def insertListToTable(self, table, items, column=0):
		for item in items:
			rowCount = table.rowCount()
			table.insertRow(rowCount)
			table.setItem(rowCount, column, QtWidgets.QTableWidgetItem(str(item)))
