from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import *
from UI.Forms.lsa_matrix_demo_form import Ui_MainWindow
from Workers import *


class LsaDemo(Ui_MainWindow, QMainWindow):
	def __init__(self):
		super(Ui_MainWindow, self).__init__()
		self.setupUi(self)
		self.open_file_button.clicked.connect(self.open_file)
		self.SVD_from_tfidf_btn.clicked.connect(self.execute_lsa_tfidf)
		self.SVD_from_freq_btn.clicked.connect(self.execute_lsa_freq)
		self.tfidf_table=self.tfidf_matrix_tablewidget
		self.freq_table=self.freq_matrix_tablewidget
		self.left_svd_table=self.Left_SVD_matrix_tablewidget
		self.values_svd_table = self.Value_SVD_matrix_tablewidget
		self.right_svd_table = self.Right_SVD_matrix_tablewidget
		
		
		
	def tables_freq_tfidf_prepare(self,table,r_count,v_header_labels,bag_of_word,items_values):
		table.setRowCount(r_count)
		table.setVerticalHeaderLabels(v_header_labels)
		
		for i in range(0, r_count):
			table.verticalHeaderItem(i).setToolTip(v_header_labels[i])
		
		table.setColumnCount(bag_of_word.num_docs)
		table.setHorizontalHeaderLabels([str(i) for i in range(0, bag_of_word.num_docs)])
		table.verticalHeader().setSectionResizeMode(QHeaderView.Interactive)
		table.verticalHeader().setTextElideMode(Qt.ElideMiddle)
		table.verticalHeader().setMaximumWidth(200)
		
		for doc_id in range(0, bag_of_word.num_docs):
			for token_in_doc in range(0, items_values[doc_id].__len__()):
				table.setItem(items_values[doc_id][token_in_doc][0], doc_id, QtWidgets.QTableWidgetItem(str(items_values[doc_id][token_in_doc][1])))
	
	
	def compute_freq_tfidf(self, inputFile):
		a = list_lines(inputFile)
		t = tokenize(a,self.delimiter,self.exclude_standalone)
		self.bw = bag_of_word(t)
		self.c = make_corpus(self.bw, t)
		self.t_id = tf_idf(self.c)
		t_count=self.bw.__len__()
		token_sorted_by_id=[self.bw[i] for i in range(0,t_count)]
		self.tables_freq_tfidf_prepare(self.freq_table,t_count,token_sorted_by_id,self.bw, self.c)
		self.tables_freq_tfidf_prepare(self.tfidf_table,t_count,token_sorted_by_id, self.bw, self.t_id)
	
		# l = lsi(t_id, bw)
	def execute_lsa_freq(self):
		if self.svd_rank_spinbox.value() > 0:
			svd_rank = self.svd_rank_spinbox.value()
		else:
			svd_rank = 2
		l = lsi(self.c, self.bw, svd_rank)
		
		right_SVD_matrix = l[self.c]  # Документ матрица
		left_SVD_matrix = l.projection.u  # Терм матрица
		values_SVD_matrix = l.projection.s  # Сингулярные значения
		
		self.values_svd_table.setColumnCount(svd_rank)
		self.values_svd_table.setRowCount(svd_rank)
		for i in range(0, svd_rank):
			self.values_svd_table.setItem(i, i, QtWidgets.QTableWidgetItem(str(values_SVD_matrix[i])))
		
		self.right_svd_table.setColumnCount(self.bw.num_docs)
		self.right_svd_table.setRowCount(svd_rank)
		self.right_svd_table.setHorizontalHeaderLabels(['Doc ' + str(i) for i in range(0, self.bw.num_docs)])
		for i in range(0, self.bw.num_docs):
			for row_num in range(0, svd_rank):
				self.right_svd_table.setItem(row_num, i, QtWidgets.QTableWidgetItem(str(right_SVD_matrix[i][row_num][1])))
		
		self.left_svd_table.setColumnCount(svd_rank)
		self.left_svd_table.setRowCount(self.bw.__len__())
		token_sorted_by_id = [self.bw[i] for i in range(0, self.bw.__len__())]
		self.left_svd_table.setVerticalHeaderLabels(token_sorted_by_id)
		self.left_svd_table.verticalHeader().setTextElideMode(Qt.ElideMiddle)
		self.left_svd_table.verticalHeader().setMaximumWidth(200)
		for i in range(0, self.bw.__len__()):
			self.left_svd_table.verticalHeaderItem(i).setToolTip(token_sorted_by_id[i])
			for column_num in range(0, svd_rank):
				self.left_svd_table.setItem(i, column_num, QtWidgets.QTableWidgetItem(str(left_SVD_matrix[i][column_num])))
		self.checkMatrix()
	
	def execute_lsa_tfidf(self):
		if self.svd_rank_spinbox.value() > 0:
			svd_rank=self.svd_rank_spinbox.value()
		else:
			svd_rank=2
		l = lsi(self.t_id, self.bw,svd_rank)
		
		right_SVD_matrix = l[self.t_id]  # Документ матрица
		left_SVD_matrix = l.projection.u # Терм матрица
		values_SVD_matrix = l.projection.s # Сингулярные значения
		
		
		pprint(left_SVD_matrix)
		self.values_svd_table.setColumnCount(svd_rank)
		self.values_svd_table.setRowCount(svd_rank)
		# token_sorted_by_id = [self.bw[i] for i in range(0, self.bw.__len__())]
		# self.values_svd_table.setVerticalHeaderLabels(token_sorted_by_id)
		# for i in range(0, r_count):
		# for i in range(0,self.bw.__len__()):
			# self.values_svd_table.verticalHeaderItem(i).setToolTip(token_sorted_by_id[i])
		for i in range(0,svd_rank):
			# if svd_rank > 1:
				# self.values_svd_table.setItem(i,i,QtWidgets.QTableWidgetItem(str(values_SVD_matrix[i][i])))
			# else:
			self.values_svd_table.setItem(i, i, QtWidgets.QTableWidgetItem(str(values_SVD_matrix[i])))
		
		
		
		
		self.right_svd_table.setColumnCount(self.bw.num_docs)
		self.right_svd_table.setRowCount(svd_rank)
		self.right_svd_table.setHorizontalHeaderLabels(['Doc '+str(i) for i in range(0, self.bw.num_docs)])
		for i in range(0, self.bw.num_docs):
			# self.right_svd_table.verticalHeaderItem(i).setToolTip(token_sorted_by_id[i])
			for row_num in range(0, svd_rank):
				self.right_svd_table.setItem(row_num, i, QtWidgets.QTableWidgetItem(str(right_SVD_matrix[i][row_num][1])))
		
		
		
		self.left_svd_table.setColumnCount(svd_rank)
		self.left_svd_table.setRowCount(self.bw.__len__())
		token_sorted_by_id = [self.bw[i] for i in range(0, self.bw.__len__())]
		self.left_svd_table.setVerticalHeaderLabels(token_sorted_by_id)
		self.left_svd_table.verticalHeader().setTextElideMode(Qt.ElideMiddle)
		self.left_svd_table.verticalHeader().setMaximumWidth(200)
		for i in range(0, self.bw.__len__()):
			self.left_svd_table.verticalHeaderItem(i).setToolTip(token_sorted_by_id[i])
			for column_num in range(0, svd_rank):
				self.left_svd_table.setItem(i, column_num, QtWidgets.QTableWidgetItem(str(left_SVD_matrix[i][column_num])))
		self.checkMatrix()
		
		
	def open_file(self):
		inputFile = QFileDialog.getOpenFileName(self, 'Открыть входной файл')[0]
		self.stemming = self.stemming_checkbox.isChecked()
		self.lowcase = self.lowcase_checkbox.isChecked()
		self.exclude_standalone = self.exclude_standalone_checkbox.isChecked()
		
		if self.comma_delimiter_radiobtn.isChecked():
			self.delimiter = ','
		elif self.space_delimiter_radiobtn.isChecked():
			self.delimiter = ' '
		elif self.custom_delimiter_radiobtn.isChecked():
			self.delimiter = self.custom_delimiter_lineEdit.text()

			
		self.compute_freq_tfidf(inputFile)
	
	def clearTable(self, table):
		table.setRowCount(0)
		table.setColumnCount(0)
	
	def insertListToTable(self, table, items, column=0):
		iterate = 0
		columns_count = table.columnCount()
		if column==columns_count:
			table.insertColumn(column)
		
		for item in items:
			rowCount = table.rowCount()
			if iterate >= rowCount:
				table.insertRow(rowCount)
			table.setItem(iterate, column, QtWidgets.QTableWidgetItem(str(item)))
			iterate += 1
		table.resizeColumnsToContents()
		
	def checkMatrix(self):
		L_SVD=self.Left_SVD_matrix_tablewidget
		L_SVD_matrix_from_table = list()
		for j in range(0, L_SVD.columnCount()):
			row=list()
			for i in range(0,L_SVD.rowCount()):
				item=L_SVD.item(i,j)
				row.append(int(item.text()))
				
			# .rowAt(0)
		
		# for col in table:
		#
		# U = np.array()