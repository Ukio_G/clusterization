import numpy
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *
from gensim import corpora
from gensim.models import Phrases
from nltk import SnowballStemmer
from numpy import unicode, zeros, asarray, log
from numpy.dual import svd
from UI.Forms.application_form import Ui_MainWindow
from UI.Forms.application_settings import Ui_Settings_form
from Workers import *
from Structures import *
from nltk.corpus import stopwords

stemmer = SnowballStemmer('russian')


class SettingsUI(Ui_Settings_form, QWidget):
	def __init__(self, application):
		super(Ui_Settings_form, self).__init__()
		self.parent = application
		self.setupUi(self)
		self.apply.clicked.connect(self.applySettings)
		self.cancel.clicked.connect(self.close)
	
	def applySettings(self):
		self.parent.settings_adapter.apply_settings()


class ApplicationUI(Ui_MainWindow, QMainWindow):
	def __init__(self, parent):
		super(Ui_MainWindow, self).__init__()
		self.setupUi(self)
		self.parent = parent
		self.settings_window = parent.settings_ui

		# Отобразить настройки
		self.showSettingsClusters_menuAction.triggered.connect(self.showSettings)
		
		# Вкладка "Входные данные"
		self.remove_selected_file_button.clicked.connect(self.remove_selected_file)
		self.show_text_button.clicked.connect(self.show_text)
		self.add_input_files_button.clicked.connect(self.select_input_files)

		# Кнопки выполнения преобразований (Вкладка предобработанные данные)
		self.texts_to_documents_button.clicked.connect(self.texts_to_documents)
		self.documents_to_dictionary_button.clicked.connect(self.documents_to_dictionary)
		self.make_corpus_button.clicked.connect(self.make_corpus)
		self.make_tf_idf_corpus_button.clicked.connect(self.make_tf_idf_corpus)
		self.make_lsi_corpus_button.clicked.connect(self.make_lsi_corpus)

		# Вывод конечного результата в OpenGL визуализаторе
		self.show_in_visualizer.clicked.connect(self.show_in_visualizer_function)

		# Меню "Файл > Открыть"
		self.open_dictionary_docs_action.triggered.connect(self.open_dictionary_docs)
		self.open_corpus_action.triggered.connect(self.open_corpus)
		self.open_tfidf_corpus_action.triggered.connect(self.open_tfidf_corpus)
		self.open_lsi_corpus_action.triggered.connect(self.open_lsi_corpus)
		self.open_tfidf_model_action.triggered.connect(self.open_tfidf_model)
		self.open_lsi_model_action.triggered.connect(self.open_lsi_model)

		# Кнопки отображения в таблицу  (Вкладка предобработанные данные)
		self.texts_to_documents_show_button.clicked.connect(self.texts_to_documents_show)
		self.show_token_to_id_button.clicked.connect(self.show_token_to_id)
		self.corpus_show_button.clicked.connect(self.corpus_show)
		self.tf_idf_corpus_show_button.clicked.connect(self.tf_idf_corpus_show)
		self.lsi_corpus_show_button.clicked.connect(self.lsi_corpus_show)
		
		# Кнопки сохранения  (Вкладка предобработанные данные)
		self.dictionary_save_button.clicked.connect(self.dictionary_save)
		self.corpus_save_button.clicked.connect(self.corpus_save)
		self.tf_idf_save_button.clicked.connect(self.tf_idf_save)
		self.lsi_save_button.clicked.connect(self.lsi_save)


	def showSettings(self):
		self.parent.settings_adapter.fill_settings_form()
		self.settings_window.show()
	

	####### Заполнение формы различными значениями ######

	## Обновление комбобокса путями файлов, которые мы выбрали
	def update_input_files_combobox(self):
		self.texts_files_combobox.clear()
		for file in self.parent.data_cache.input_files:
			self.texts_files_combobox.addItem(file.file_path)

	## Вывод в таблицу всех документов
	def update_table_with_documents(self):
		self.clearTable(self.preprocessed_data_table)
		for document in self.parent.data_cache.documents:
			self.list_to_table_column(self.preprocessed_data_table,document)

	## Вывод в таблицу Corpora dictionary
	def update_table_with_dictionary(self):
		self.clearTable(self.preprocessed_data_table)

	def update_table_with_lsi_words(self):
		self.clearTable(self.preprocessed_data_table)
		for item in self.parent.data_cache.lsi_corpus:
				pprint(item)
				self.list_to_table_column(self.preprocessed_data_table,item)

	## Вывод в таблицу цифрового корпуса

	def update_table_with_corpus(self):
		self.clearTable(self.preprocessed_data_table)
		for item in self.parent.data_cache.corpus:
			pprint(item)
			self.list_to_table_column(self.preprocessed_data_table,item)

	def update_table_with_tf_idf_corpus(self):
		self.clearTable(self.preprocessed_data_table)
		for item in self.parent.data_cache.tf_idf_corpus:
			pprint(item)
			self.list_to_table_column(self.preprocessed_data_table,item)
	#Работа с таблицами
	
	def clearTable(self, table):
		table.setRowCount(0)
		table.setColumnCount(0)
	
	def list_to_table_column(self, table, items):
		columns = table.columnCount()
		rows = table.rowCount()
		rows_need = items.__len__() - rows
		for item in items:
			table.setColumnCount(columns+1)
			
			if rows_need > 0:
				table.setRowCount(items.__len__())

		i = 0 
		for item in items:
			table.setItem(i,columns,QTableWidgetItem(str(item)))
			i = i + 1
			

	# Кнопки формы

	def select_input_files(self):
		input_files = QFileDialog.getOpenFileNames(self.parent.ui,'Открыть входной файл')[0]
		for file in input_files:
			self.parent.data_cache.input_files.append(InputFile(file))
		
		self.update_input_files_combobox()

	def remove_selected_file(self):
		if self.texts_files_combobox.count() > 0:
			current_index = self.texts_files_combobox.currentIndex()
			print(current_index)
			print(self.parent.data_cache.input_files[current_index].file_path)
			del self.parent.data_cache.input_files[current_index]
			self.update_input_files_combobox()

	def show_text(self):
		if self.texts_files_combobox.count() > 0:
			self.text_content.clear()
			current_index = self.texts_files_combobox.currentIndex()
			filename_selected = self.parent.data_cache.input_files[current_index].file_path
			with open(filename_selected) as f:
				for line in f:
					self.text_content.appendPlainText(line)

	# Кнопки вывода промежуточных данных в таблицу (Вкладка предобработанные данные)
	def texts_to_documents_show(self):
		self.update_table_with_documents()

	def show_token_to_id(self):
		pprint(self.parent.data_cache.dictionary.token2id)

	def corpus_show(self):
		self.update_table_with_corpus()

	def tf_idf_corpus_show(self):
		self.update_table_with_tf_idf_corpus()
			
	def lsi_corpus_show(self):
		self.update_table_with_lsi_words()

	# Вывод конечного результата в OpenGL визуализаторе
	def show_in_visualizer_function(self):
		self.parent.visualizate_data()

	# Кнопки выполнения преобразований (Вкладка предобработанные данные)
	def texts_to_documents(self):
		if self.parent.data_cache.input_files.__len__() > 0:
			docs_list = filenames_to_documents_dictionary(self.parent.input_files_list())
			if self.parent.settings.is_exclude_minimum_frequency:
				docs_list = frequency_filter(docs_list)
			self.parent.data_cache.documents = docs_list

	def documents_to_dictionary(self):
		if self.parent.data_cache.documents.__len__() > 0:
			self.parent.data_cache.dictionary = corpora.Dictionary(self.parent.data_cache.documents)

	def make_corpus(self):
		self.parent.data_cache.corpus.filenames = self.parent.input_files_list()
		self.parent.data_cache.corpus.dictionary = self.parent.data_cache.dictionary

	def make_tf_idf_corpus(self):
		self.parent.data_cache.tf_idf_model = models.TfidfModel(self.parent.data_cache.corpus)
		self.parent.data_cache.tf_idf_corpus = self.parent.data_cache.tf_idf_model[self.parent.data_cache.corpus]

	def make_lsi_corpus(self):
		self.parent.data_cache.lsi_model = models.LsiModel(self.parent.data_cache.tf_idf_corpus, id2word=self.parent.data_cache.dictionary, num_topics=3)
		self.parent.data_cache.lsi_corpus = self.parent.data_cache.lsi_model[self.parent.data_cache.tf_idf_corpus]


	# Открытие файлов (Словари, различные типы корпусов, модели) (Меню Файл > Открыть)

	def open_dictionary_docs(self):
		input_file = QFileDialog.getOpenFileName(self.parent.ui,'Открыть входной файл')[0]
		if input_file != "":
			self.parent.data_cache.dictionary = corpora.Dictionary.load_from_text(input_file)
			pprint(self.parent.data_cache.dictionary.id2token)
		
	def open_corpus(self):
		input_file = QFileDialog.getOpenFileName(self.parent.ui,'Открыть входной файл')[0]
		if input_file != "":
			self.parent.data_cache.corpus = corpora.MmCorpus(input_file)

	def open_tfidf_corpus(self):
		input_file = QFileDialog.getOpenFileName(self.parent.ui,'Открыть входной файл')[0]
		if input_file != "":
			self.parent.data_cache.tf_idf_corpus = corpora.MmCorpus(input_file)

	def open_lsi_corpus(self):
		input_file = QFileDialog.getOpenFileName(self.parent.ui,'Открыть входной файл')[0]
		if input_file != "":
			self.parent.data_cache.lsi_corpus = corpora.MmCorpus(input_file)

	def open_tfidf_model(self):
		input_file = QFileDialog.getOpenFileName(self.parent.ui,'Открыть входной файл')[0]
		if input_file != "":
			self.parent.data_cache.tf_idf_model = models.TfidfModel.load(input_file)
			
	def open_lsi_model(self):
		input_file = QFileDialog.getOpenFileName(self.parent.ui,'Открыть входной файл')[0]
		if input_file != "":
			self.parent.data_cache.lsi_model = models.LsiModel.load(input_file)
	# Кнопки сохранения

	def dictionary_save(self):
		output_file = QFileDialog.getSaveFileName(self.parent.ui,'Сохранить файл', self.parent.settings.result_files_directory, 'Dictionary (*.dict)')[0]
		if output_file != "":
			self.parent.data_cache.dictionary.save_as_text(output_file)

	def corpus_save(self):
		output_file = QFileDialog.getSaveFileName(self.parent.ui,'Сохранить файл', self.parent.settings.result_files_directory, 'MatrixMarket (*.mm)')[0]
		if output_file != "":
			self.parent.data_cache.corpus.save(output_file)

	def tf_idf_save(self):
		output_file = QFileDialog.getSaveFileName(self.parent.ui,'Сохранить файл корпуса', self.parent.settings.result_files_directory, 'MatrixMarket (*.mm)')[0]
		if output_file != "":
			corpora.MmCorpus.serialize(output_file,self.parent.data_cache.tf_idf_corpus)
		self.tf_idf_model_save()

		
	def lsi_save(self):
		output_file = QFileDialog.getSaveFileName(self.parent.ui,'Сохранить файл', self.parent.settings.result_files_directory, 'MatrixMarket (*.mm)')[0]
		if output_file != "":
			corpora.MmCorpus.serialize(output_file,self.parent.data_cache.lsi_corpus)

		self.lsi_model_save()

	def tf_idf_model_save(self):
		output_file = QFileDialog.getSaveFileName(self.parent.ui,'Сохранить файл', self.parent.settings.result_files_directory, 'Gensim models (*.model)')[0]
		if output_file != "":
			self.parent.data_cache.tf_idf_model.save(output_file)
		
	def lsi_model_save(self):
		output_file = QFileDialog.getSaveFileName(self.parent.ui,'Сохранить файл', self.parent.settings.result_files_directory, 'Gensim models (*.model)')[0]
		if output_file != "":
			self.parent.data_cache.lsi_model.save(output_file)

	