from PyQt5.QtWidgets import QApplication
from UI.application_ui import *
from Structures import *
import sys
from os.path import expanduser
import configparser
import random

class Application:
	"""Класс приложения, связывает между собой отдельные модули, механику работы приложения и графический интерфейс, корректно запускает и завершает работу приложения"""
	__instance = None
	__inited = False
	__is_visualizer_opened = False
	def __new__(cls):
		if Application.__instance is None:
			Application.__instance = super().__new__(cls)
		return Application.__instance

	def __init__(self):
		if Application.__inited is True:
			return
		from Structures import Settings, SettingsAdapter
		
		self.q_app = QApplication(sys.argv)
		self.settings = Settings(self,'.lsi.config')
		self.settings_ui = SettingsUI(self)
		self.settings_adapter = SettingsAdapter(self.settings_ui ,self.settings)
		self.ui = ApplicationUI(self)
		self.data_cache = DataCache(self)
		Application.__inited = True

	def start(self):
		self.ui.show()
		sys.exit(self.q_app.exec_())

	def exit(self):
		self.settingsUI.close()
		self.ui.close()

	def input_files_list(self):
		result = []
		for file in self.data_cache.input_files:
			result.append(file.file_path)
		return result

	def visualizate_data(self):
		# if __is_visualizer_opened is True:
		# 	print("Visualizer already opened")
		# 	return

		from pprint import pprint

		pprint(self.data_cache.dictionary.id2token)

		
		data = dict()
		scale = 10
		noise = 0.0
		for i in self.data_cache.dictionary.token2id:
			_id = self.data_cache.dictionary.token2id[i]
			_token_str = i
			base_position = self.data_cache.lsi_model.projection.u[_id]
			x = (base_position[3] * scale) + random.uniform(-noise,noise)
			y = (base_position[4] * scale) + random.uniform(-noise,noise)
			z = (base_position[5] * scale) + random.uniform(-noise,noise)
			data[_token_str] = [x,y,z] 

		from Visualizer.Render import thread_wrapper
		import threading

		
		loop_thread = threading.Thread(target=thread_wrapper,args=(data,))
		loop_thread.start()		



			# print("word is ", self.data_cache.dictionary.token2id)
		# for i in range()
		# V = gensim.matutils.corpus2dense(lsi_model[])

