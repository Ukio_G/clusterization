from enum import Enum


class Type(Enum):
	single = 1
	n_gram = 2
	array_single = 3
	array_n_gram = 4
	array_mixed = 5
	unknown = 0
	
	def whichType(self, item):
		if isinstance(item, str):
			item.strip(' ')
			if item.find(' ') == -1:
				return self.single
			elif item.find(' ') > 0:
				return self.n_gram
		elif isinstance(item, set):
			return self.array_mixed
